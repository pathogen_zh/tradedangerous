#! /usr/bin/env python

raise SystemExit(
    "Use trade.py station [--add | --update | --remove] instead."
)
